Execute follow commands to turn up the stack:

docker swarm init --advertise-addr $(hostname -i)

docker stack deploy --compose-file=docker-compose.yml tomcat-stack